#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>
#include <QDebug>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left = nullptr;
        this->right = nullptr;
        this->value = value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if (value < this->value)
        {
            if (this->left == nullptr)
            {
                this->left = createNode(value);
            }
            else
            {
                this->left->insertNumber(value);
            }
        }
        else
        {
            if (this->right == nullptr)
            {
                this->right = createNode(value);
            }
            else
            {
                this->right->insertNumber(value);
            }
        }

        /* POUR L'EQUILIBRAGE MAIS INCOMPLET
        Node* start = nullptr;
        if (this->height() == 1 && start == nullptr)
        {
            start = this;
        }
        if (start->nodesCount() <= pow(2, start->height() - 1) - 1)
        {
            qDebug() << start->height();
        }*/
    }

    uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint leftCount, rightCount;

        if (this->left)
        {
            leftCount = this->left->height();
        }
        else
        {
            leftCount = 0;
        }

        if (this->right)
        {
            rightCount = this->right->height();
        }
        else
        {
            rightCount = 0;
        }

        if (leftCount > rightCount)
        {
            return leftCount + 1;
        }
        else
        {
            return rightCount + 1;
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint sum = 0;

        if (this->left)
        {
            sum += this->left->nodesCount();
        }

        if (this->right)
        {
            sum += this->right->nodesCount();
        }

        return sum + 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (this->left || this->right)
        {
            return false;
        }
        else
        {
            return true;
        }
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf())
        {
            leaves[leavesCount] = this;
            leavesCount += 1;
        }
        else
        {
            if (this->left)
            {
                this->left->allLeaves(leaves, leavesCount);
            }

            if (this->right)
            {
                this->right->allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (this->left)
        {
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount += 1;

        if (this->right)
        {
            this->right->inorderTravel(nodes, nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount += 1;

        if (this->left)
        {
            this->left->preorderTravel(nodes, nodesCount);
        }

        if (this->right)
        {
            this->right->preorderTravel(nodes, nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (this->left)
        {
            this->left->postorderTravel(nodes, nodesCount);
        }

        if (this->right)
        {
            this->right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount += 1;
	}

	Node* find(int value) {
        // find the node containing value
        if (this->value == value)
        {
            return this;
        }
        else
        {
            if (value > this->value)
            {
                this->right->find(value);
            }
            else
            {
                this->left->find(value);
            }
        }
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
