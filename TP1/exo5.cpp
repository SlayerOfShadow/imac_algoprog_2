#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    if (n == 0)
    {
        return 0;
    }
    else
    {
        Point temp;
        temp.x = pow(z.x, 2) - pow(z.y, 2) + point.x;
        temp.y = 2 * z.x * z.y + point.y;
        if (temp.length() < 2)
        {
            return isMandelbrot(temp, n - 1, point);
        }
        else
        {
            return n;
        }
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



