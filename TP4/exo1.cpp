#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>
#include <QDebug>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && (*this)[i] > (*this)[(i-1)/2])
    {
        int temp = (*this)[i];
        (*this)[i] = (*this)[(i-1)/2];
        (*this)[(i-1)/2] = temp;
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{

    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max;
    int val1 = (*this)[nodeIndex];

    int val2;
    if (leftChild(nodeIndex) < heapSize ? val2 = (*this)[leftChild(nodeIndex)] : val2 = -1);

    int val3;
    if (rightChild(nodeIndex) < heapSize ? val3 = (*this)[rightChild(nodeIndex)] : val3 = -1);

    if (val1 > val2 && val1 > val3)
    {
        i_max = nodeIndex;
    }
    else if (val2 > val1 && val2 > val3)
    {
        i_max = leftChild(nodeIndex);
    }
    else
    {
        i_max = rightChild(nodeIndex);
    }

    if (i_max != nodeIndex)
    {
        int temp = (*this)[nodeIndex];
        (*this)[nodeIndex] = (*this)[i_max];
        (*this)[i_max] = temp;
        heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (int i = 0; i < numbers.size(); i++)
    {
        insertHeapNode(numbers.size(), numbers[i]);
    }
}

void Heap::heapSort()
{
    for (int i = this->size() - 1; i > 0; i--)
    {
        int temp = (*this)[0];
        (*this)[0] = (*this)[i];
        (*this)[i] = temp;
        heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
